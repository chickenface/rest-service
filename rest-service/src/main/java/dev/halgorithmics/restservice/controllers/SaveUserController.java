package dev.halgorithmics.restservice.controllers;

import dev.halgorithmics.restservice.models.User;
import dev.halgorithmics.restservice.services.UserService;
import dev.halgorithmics.restservice.utils.DBUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class SaveUserController {

    private final UserService service;

    public SaveUserController() throws SQLException {
        this.service = new UserService();
    }

    @PostMapping("/save")
    public void saveUser(@RequestBody final User user) {
        //user.password = DBUtil.encrypt(user.password);
        user.age = calculateAge(user.bornDate);
        this.service.addUser(user);
    }

    private int calculateAge(Date given) {
        Date current = new Date();
        int difference = current.getYear() - given.getYear();
        return difference;
    }

}
