package dev.halgorithmics.restservice.controllers;

import dev.halgorithmics.restservice.models.User;
import dev.halgorithmics.restservice.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class GetUserController {

    UserService service;

    public GetUserController() throws SQLException {
        this.service = new UserService();
    }

    @GetMapping("/login")
    public ResponseEntity<User> save(@RequestParam String username, @RequestParam String password) {
        User user = new User();
        user.name = username;
        user.password = password;
        ResponseEntity<User> onErrorResponse = ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new User());

        if (!this.validation(user)) {
            return onErrorResponse;
        }

        user = this.service.getUserByName(user.name);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(user);
    }

    private boolean validation(User user) {
        boolean checkContent = Objects.equals(user.email, "") || Objects.equals(user.password, "");
        boolean checkLength = user.password.length() < 8;
        boolean checkUppercase = !hasUppercase(user.password);
        boolean checkSpecialChars = !(hasSpecialCharacter(user.password));
        boolean checkNumber = !(hasNumber(user.password));

        return !checkContent && !checkLength && !checkUppercase && !checkSpecialChars && !checkNumber;
    }
    private boolean hasUppercase(String value) {
        return value.matches(".*[A-Z].*");
    }

    public boolean hasSpecialCharacter(String str) {
        return str.matches(".*[^a-zA-Z0-9].*");
    }

    public boolean hasNumber(String str) {
        return str.matches(".*\\d.*");
    }


}
