package dev.halgorithmics.restservice.models;

import java.time.LocalDate;
import java.util.Date;

public class User {
    public String idNumber;
    public String idType;
    public Date bornDate;
    public int age;
    public String phoneNumber;
    public String email;
    public String name;
    public String password;

    @Override
    public String toString() {
        return "User{" +
                "idNumber='" + idNumber + '\'' +
                ", idType='" + idType + '\'' +
                ", bornDate=" + bornDate +
                ", age=" + age +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
