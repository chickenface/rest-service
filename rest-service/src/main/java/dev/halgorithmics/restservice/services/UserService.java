package dev.halgorithmics.restservice.services;

import dev.halgorithmics.restservice.models.User;
import dev.halgorithmics.restservice.utils.DBUtil;
import org.springframework.stereotype.Service;

import java.sql.*;

@Service
public class UserService {

    Connection connection;

    public UserService() throws SQLException {
        connection = DBUtil.getConnection();
    }
    public void addUser(User user) {
        String query = "INSERT INTO users (idNumber, idType, bornDate, age, phoneNumber, email, name, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement statement = null;
            int year = user.bornDate.getYear();
            int month = user.bornDate.getMonth();
            int day = user.bornDate.getDay();
            java.sql.Date convertedDate = new java.sql.Date(year, month, day);
            statement = connection.prepareStatement(query);
            statement.setString(1, user.idNumber);
            statement.setString(2, user.idType);
            statement.setDate(3, convertedDate);
            statement.setInt(4, user.age);
            statement.setString(5, user.phoneNumber);
            statement.setString(6, user.email);
            statement.setString(7, user.name);
            statement.setString(8, user.password);

            int affected = statement.executeUpdate();
            if (!(affected > 0)) {
                throw new SQLException("no affected rows");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User getUserByName(String name) {
        User result = new User();
        String query = "SELECT * FROM users WHERE name = ?";

        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                result.idNumber = set.getString("idNumber");
                result.idType = set.getString("idType");
                result.age = set.getInt("age");
                result.phoneNumber = set.getString("phoneNumber");
                result.bornDate = set.getDate("bornDate");
                result.email = set.getString("email");
                result.name = set.getString("name");
                result.password = set.getString("password");
            } else {
                throw new SQLException("not found result");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
