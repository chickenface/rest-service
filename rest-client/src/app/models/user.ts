export interface User {
    /**
     * numero de identificacion (cedula)
     */
    idNumber: string;

    /**
     * tipo de documento de identificacion
     */
    idType: string;

    /**
     * ano en que nacio
     */
    bornDate: Date;

    /**
     * edad
     */
    age?: number;

    /**
     * numero de telefono
     */
    phoneNumber: string;

    /**
     * correo electronico
     */
    email: string;

    /**
     * nombre de usuario
     */
    name: string;

    /**
     * clave
     */
    password: string;
}
