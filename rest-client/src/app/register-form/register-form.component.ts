import { Component, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { User } from '../models/user';
import { UserService } from '../user.service';
import { ModalComponent } from '../components/modal/modal.component';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {

  account = this.builder.group({
    name: ['', [Validators.required]],
    email: ['', [
      Validators.email,
      Validators.required,
    ]],
    idNumber: ['', [Validators.required]],
    idType: ['', [Validators.required]],
    bornDate: [new Date(), [Validators.required]],
    phoneNumber: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  @ViewChild('modal')
  modal!: ModalComponent;

  currentStep = 1;

  constructor(
    private readonly builder: FormBuilder,
    private service: UserService,
  ) {}

  register() {
    if (this.account.valid) {
      const { value } = this.account;
      const upload: User = {
        name: value.name!,
        email: value.email!,
        idNumber: value.idNumber!,
        idType: value.idType!,
        bornDate: value.bornDate!,
        phoneNumber: value.phoneNumber!,
        password: value.password!,
      };

      this.service.registerUser(upload).subscribe({
        next: () => {
          // this.router.navigate(['/login']);
          this.currentStep = 1;
          this.modal.open('Registro', 'El registro ha sido exitoso', 'done');
        },
        error: (err) => {
          console.log(err);
          this.modal.open('Error', 'Ha ocurrido un error al procesar su peticion', 'error')
        }
      });
      return;
    }
    
    this.modal.open('Error', 'El fomulario no es valido', 'error')
  }
}
