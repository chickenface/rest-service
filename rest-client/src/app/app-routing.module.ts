import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { NameFormComponent } from "./name-form/name-form.component";
import { PasswordFormComponent } from "./password-form/password-form.component";
import { RegisterFormComponent } from "./register-form/register-form.component";

const routes: Routes = [
    {
        path: 'login',
        component: NameFormComponent,
    },
    {
        path: 'password',
        component: PasswordFormComponent,
    },
    {
        path: 'register',
        component: RegisterFormComponent,
    },
    {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
