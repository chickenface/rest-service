import { Injectable } from '@angular/core';
import { User } from './models/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly address = 'http://localhost:8080';

  logged = {
    name: '',
    password: '',
  };

  constructor(private http: HttpClient) { }

  /**
   * enviar datos al servidor para guardar en base de datos.
   * @param user datos de usuario
   * @returns http objeto con el status
   */
  registerUser(user: User) {
    return this.http.post<void>(`${this.address}/save`, user);
  }

  /**
   * metodo para logearse en el sistema
   * @param username nombre de usuario
   * @param password contrasena del usuario en texto plano
   * @returns objeto de usuario con una propiedad adicional que lleva el token de login
   */
  login(username: string, password: string) {
    return this.http.get<User>(`${this.address}/login?username=${username}&password=${password}`);
  }
}
