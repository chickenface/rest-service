import { Component, ElementRef, Input, OnChanges, QueryList, ViewChildren } from '@angular/core';


@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnChanges {

  @Input()
  step: number = 1;

  @ViewChildren('steps')
  circles!: QueryList<ElementRef<HTMLSpanElement>>;

  width: string = '0%';

  ngOnChanges(): void {
    this.width = this.getWidth();
  }

  getWidth(): string {
    const width = ((this.step - 1) / (3 - 1)) * 100;
    console.log(width);
    
    return `${width}%`;
  }
}
