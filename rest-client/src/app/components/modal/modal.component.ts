import { Component } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  title: string = '';
  message: string = '';
  icon: string = '';
  isOpen = false;

  open(title: string, message: string, icon: string) {
    this.title = title;
    this.message = message;
    this.icon = icon;
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;    
  }

}
