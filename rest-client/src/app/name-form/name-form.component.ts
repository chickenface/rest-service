import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-name-form',
  templateUrl: './name-form.component.html',
  styleUrls: ['./name-form.component.scss']
})
export class NameFormComponent {

  control = new FormControl('', Validators.required);

  constructor(
    private service: UserService,
    private router: Router,
  ) {}

  submit() {
    if (this.control.valid) {
      const { value } = this.control;
      this.service.logged.name = value!;
      this.router.navigate(['/password']);
      console.log(this.service.logged);
      
    }
  }
}
