import { Component, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { FormControl, Validators } from '@angular/forms';
import { ModalComponent } from '../components/modal/modal.component';

@Component({
  selector: 'app-password-form',
  templateUrl: './password-form.component.html',
  styleUrls: ['./password-form.component.scss']
})
export class PasswordFormComponent {

  private readonly uppercase = /.*[A-Z].*/;
  private readonly specialchars = /.*[^a-zA-Z0-9].*/;

  control = new FormControl('', [
    Validators.required, 
    Validators.minLength(8),
    Validators.pattern(this.uppercase),
    Validators.pattern(this.specialchars),
  ]);

  @ViewChild('modal')
  modal!: ModalComponent;

  constructor(private service: UserService) {
    console.log(this.service.logged)
  }

  submit() {
    const { value } = this.control;
    this.service.logged.password = value!;
    const { name, password } = this.service.logged;
    this.service.login(name, password).subscribe({
      next: (res) => {
        this.modal.open('Ingreso', 'Ingreso exitoso!', 'done');
        console.log(res);
      },
      error: () => this.modal.open('Error', 'Ha ocurrido un error en el servidor, intente mas tarde', 'error'),
    });
  }
}
